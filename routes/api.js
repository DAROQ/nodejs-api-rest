const express = require('express');

const router = express.Router();

// get a list of ninjas from de DB
router.get('/ninjas', function(request, response) {
    response.send({ type: 'GET' });    
});

// add a new ninja to the db
router.post('/ninjas', function(request, response) {
    response.send({ type: 'POST' });    
});

// update a ninja in the DB
router.put('/ninjas/:id', function(request, response) {
    response.send({ type: 'PUT' });    
});

// delete a ninja from the DB
router.delete('/ninjas/:id', function(request, response) {
    response.send({ type: 'DELETE' });    
});

module.exports = router;
