const express = require('express');
const apiRoutes = require('./routes/api');

// Set up express app
const app = express();

app.use(apiRoutes);

// Set up the port
app.listen(3000, function(){
    console.log("Ahora estamos escuchando peticiones en el puerto 3000");
});